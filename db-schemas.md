# Database Schemas
## Schemas
- Admin
- Particiapnt
- College
- Event

# Elaboration
## Admin Schema

Fields in admin schema and its details <br>

| Name     | Type   | Description           | Required? | Remarks |
|----------|--------|-----------------------|----------|---------|
| username | String |                       | Yes      |         |
| hashword | String | Initial password hash | Yes      |         |
|          |        |                       |          |         |

## Participant Schema

Fields in participant schema and its details <br>

| Name       | Type    | Description                                                                                                                                                                                 | Required?   | Remarks                                                      |
|------------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|--------------------------------------------------------------|
| authClass  | String  | Identifies mode of authentication                                                                                                                                                           | Initially  | Values - 'google', 'local'                                   |
| authId     | String  | ID of the user in the given mode of authentication in authClass     | Initially  | Need not be unique *                                         |
| hashword   | String  | Initial password hash for local authentication.  Left empty for other modes like google.                                                                                                    | No         |                                                              |
| name       | String  | Name of the participant                                                                                                                                                                     | Fill later |                                                              |
| college_id | String  | Identifier of the college to which participant belong                                                                                                                                       | Fill later | Another collection stores all college names with identifiers |
| email      | String  | Email id of the participant                                                                                                                                                                 | Fill later |                                                              |
| mobile     | String  | Mobile number                                                                                                                                                                               | Fill later | Numeric String                                               |
| gender     | String  | Gender                                                                                                                                                                                      | Fill later | Values - 'male', 'female', 'other'                           |
| complete (TODO)   | Boolean | Marks weather necessary fields are filled. Necessary fields to be filled after login are -  name, college_id, email, gender. This should be true to enable participants register to events. | Yes        | This is a TODO         |

\* *Both fileds authClass and authID when kept together should be unique. But there could be case of same authID for different authClass.*

## College Schema
Fields in college schema and its details

| Name | Type | Description | Required? | Remarks |
|--|--|--|--|--|
| _id | string | Automatically generated id of an entry. | Auto | |
| name | string | Unique and extended name of college. Eg. National Institute of Technology Calicut, Kerala | Yes | Duplicates are not allowed |


## Event Schema
Fields in event schema and its details

| Name | Type | Description | Required? | Remarks |
|--|--|--|--|--|
| _id | string | Autogenerated ID of entry | Auto | |
| name | string | Name of event | Yes | Duplicates not allowed |
| category | string | Category of event | No | |
| description | string | Describe the event | No | |
| meta | string | Meant to store a json string representing extra data of event. | No | |
| booking_limit | number | Upper limit on number of bookings allowed for the event | Fill later | |
| published | boolean | Indicates weather event is published or not. | Has default | |
| booked_count | number | Number of participants registered | Has default | |

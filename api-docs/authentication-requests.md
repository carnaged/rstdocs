
# Authentication Requests

<blockquote>
Please note that the endpoints given below are relative to the root where proton is hosted. <br>
Also all data are send and recieved as json.
</blockquote>


## Participant Registration
### POST <code>/auth/participant/register</code>
New participants can be registered through this endpoint. Once registered, another endpoint can be used to sign in the participant using the credentials provided here to obtain a token that identifies the participant. All new participants are required to register once.

#### Request Body Parameters

| Field | DataType | Description |
|----------|----------|-------------|
| username | String   | Username to register as |
| password | String   | Password to register with |

#### Example request body
```
{
    "username": "johndoe",
    "password": "supersecret"
}
```
#### Example responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "message": "Participant johndoe has been registered"
  }
  ```
- Failure when the username is already taken <br>
  *Status* 400 Bad Request
  ```
  {
  "message": "E11000 duplicate key error collection: protondev.authparticipants index: authID_1 dup key: { : "johndoess" }",
  "status": 400,
  "stack": "MongoError: E11000 duplicate key error collection: protondev.authparticipants index: authID_1 dup key: { : "johndoess" } at Function.create (/home/salih/Projects/proton/node_modules/mongodb-core/lib/error.js:43:12) at toError (/home/salih/Projects/proton/node_modules/mongodb/lib/utils.js:149:22) at coll.s.topology.insert (/home/salih/Projects/proton/node_modules/mongodb/lib/operations/collection_ops.js:859:39) at /home/salih/Projects/proton/node_modules/mongodb-core/lib/connection/pool.js:397:18 at process._tickCallback (internal/process/next_tick.js:61:11)"
  }
  ```

## Get Participant Token

### POST <code>/auth/participant/get-token</code>

Most participant related requests requires authentication. This is possible by sending a signed token string as *Authentication Bearer Token* in the header of every such request. A participant token can be obtained from this endpoint by sending their credentials. The front-end should store this token localy inorder to make use of it.

### Request Body Parameters

| Field | Type | Description |
|--|--|--|
| username | string | Username registered under |
| password | string | Password registered with |

#### Example Request Body
```
{
  "username": "johndoe",
  "password": "supersecret"
}
```

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "username": "johndoe",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG5kb2UiLCJpYXQiOjE1NTg1Mzg4MTZ9.xztpI4wO78gwNDu9OBHnxUf-pWeAT2tVdtYN6usFm8Q"
  }
- Failure on Invalid Username<br>
  *Status* 500 Internal Server Error
  ```
  {
    "status": 500
  }
  ```
- Failur on Invalid Password <br>
  *Status* 500 Internal Server Error
  ```
  {
    "status": 500
  }
  ```

## Get Admin Token
### POST <code>/auth/admin/get-token</code>

To be able to manage events, organisers should register as admin manually upon setting up proton. Registered admins can get a token by providing their credentials. This token should be send as *Authentication Bearer Token* in header of requests requiring admin access.

### Request Body Parameters

| Field | Type | Description |
|--|--|--|
| username | string | Registered username of admin |
| password | string | Password registered with |

#### Example Request Body
```
{
  "username": "johndoe_senior",
  "password": "supersupersecret"
}
```
#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "username": "johndoe_senior",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImpvaG5kb2Vfc2VuaW9yIiwiaWF0IjoxNTU4NTQwNDU3fQ.M5vga95lhG-reNz1UAFQI1F6nV56UUenL_FRD45JDz4"
  }
  ```
- Failure on Invalid Username<br>
  *Status* 500 Internal Server Error
  ```
  {
    "status": 500
  }
  ```
- Failur on Invalid Password <br>
  *Status* 500 Internal Server Error
  ```
  {
    "status": 500
  }
  ```


# Colleges Management Requests

<blockquote>
Please note that the endpoints given below are relative to the root where proton is hosted. <br>
Also all data are send and recieved as json.
</blockquote>

## Add College
### POST <code>/colleges/</code>
A new college can be added to the list of colleges using this request. The name of the college is provided in body. Once added, an id is generated for the college and can be retrieved via Get Colleges Request.

### Request Body Parameters

| Field | Type | Description | Required? |
|--|--|--|--|
| name | string | Unique extended name of the college | Yes |

#### Example Request Body
```
{
  "name": "MES College of Engineering, Kallanthode"
}
```
#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "message": "New college added with id 5ce5950c1f299421d504408d"
  }
  ```
- Failure when a college with same name exist <br>
  *Status* 400 Bad Request
  ```
  {
    "message": "E11000 duplicate key error collection: protondev.colleges index: name_1 dup key: { : "MES College of Engineering, Kallanthode" }",
    "status": 400,
    "stack": "MongoError: E11000 duplicate key error collection: .....(internal/process/next_tick.js:61:11)"
  }
  ```
- Failure when name is not provided <br>
  *Status* 400 Bad Request
  ```
  {
    "message": "College name not specified",
    "status": 400,
    "stack": "ReferenceError: College name not specified at addCollege (/home/salih/Projects/proto......index.js:635:15"
  }
  ```

## Get Colleges
### GET <code>/colleges/</code>
Returns a json object containing property 'colleges' which holds an array of all college objects each with its name and id.

#### Example Request
URL <code>/colleges/</code>

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "colleges": [
      {
        "_id": "5ce5950c1f299421d504408d",
        "name": "MES College of Engineering, Kallanthode"
      },
      {
        "_id": "5ce596a61f299421d504408f",
        "name": "MAMO College Manasserry, Calicut"
      }
    ]
  }
  ```

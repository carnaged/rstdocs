
# Events Management Requests

<blockquote>
Please note that the endpoints given below are relative to the root where proton is hosted. <br>
Also all data are send and recieved as json.
</blockquote>

## Add Event
### POST <code>/events/</code>
Adds a new event with given details into the list of events
### Request Body Parameters

| Field | Type | Description | Required? |
|--|--|--|--|
| name | string | Name of the new event | Yes |
| category | string | Eg. 'workshop', 'lecture' | No |
| description | string | Describe the event | No |
| meta | json string | To store extra data about the event Eg. speaker | No |
| booking_limit | number | Upper limit on number of bookings allowed | Fill before publishing |

#### Example Request Body
```
{
  "name": "Crypto-currency and Blockchains",
  "category": "workshop",
  "description": "A long string",
  "meta": "{ \"speaker\": \"John Doe\", \"venue\": \"SSL\" }",
  "booking_limit": 20
}
```

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "message": "New event added",
    "event": {}
  }
  ```
- Bug when an existing event name is used to register <br>
  *Status* 200 OK
  ```
  {
    "message": "New event added",
    "event": {}
  }
  ```
  The response indicates success but this new entry is dropped by the database owing to duplicate name.

## Put Event
### PUT <code>/events/:id</code>
The entire editable body of an event will be edited with this put request. The id of the event to be edited is supplied as a url parameter as shown in the endpoint above. If a field is missing in the request body, its corresponding database cell will be set empty.

### Request Body Parameters

*Same as Add Event Request*

| Field | Type | Description | Required? |
|--|--|--|--|
| name | string | Name of the new event | Yes |
| category | string | Eg. 'workshop', 'lecture' | No |
| description | string | Describe the event | No |
| meta | json string | To store extra data about the event Eg. speaker | No |
| booking_limit | number | Upper limit on number of bookings allowed | Fill before publishing |

#### Example Request Body
To url <code>/events/5cdf58b3ef30d8125c4f2c9c</code><br>
```
{
  "name": "Crypto-currency and Blockchains",
  "category": "workshop",
  "description": "A long string",
  "meta": "{ \"speaker\": \"John Doe\", \"venue\": \"SSL\" }",
  "booking_limit": 20
}
```

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "message": "Updated",
    "event": {
      "booked_count": 0,
      "published": true,
      "_id": "5cdf58b3ef30d8125c4f2c9c",
      "name": "Crypto-currency and Blockchains",
      "__v": 0,
      "pnt_cnt": 50,
      "pnt_lim": 89,
      "desc": null,
      "type": "Workshop",
      "meta": "{ "speaker": "John Doe", "venue": "SSL" }",
      "booking_limit": 20,
      "description": "A long string",
      "category": "workshop"
    }
  }
- Bug when there is no event with given id <br>
  *Status* 200 OK
  ```
  {
    "message": "Updated",
    "event": null
  }
  ```
- Failure on invalid id provided in url parameter <br>
  *Status* 500 Internal Server Error
  ```
  {
    "message": "Cast to ObjectId failed for value "5cdf58b3ef30d8125c2c9c" at path "_id" for model "event"",
    "status": 500,
    "stack": "CastError: Cast to ObjectId failed for value "5cdf58b3ef30d8125c2c9c" at path "_id" for model "event" at new ..... CastError (/home/salih/Projects/node_modules/mongoose/lib/process/next_tick.js:61:11)"
  }
  ```

## Get Events
### GET <code>/events?category=\<category\></code>
The list of added events can be obtained by this request. It will return both published and unpublished events. Additionally, the result can be filtered based on category using the category query parameter.

### Optional Request Query Parameters

| Parameter | Description |
|--|--|
| category | Filters result based on categoy Eg. workshop |

#### Example Request
URL <code>/events?category=workshop</code>

#### Example Responses
- Success
  *Status* 200 OK
  ```
  [
    {
      "_id": "5cdf58b3ef30d8125c4f2c9c",
      "name": "MEAN Stack",
      "category": "workshop"
    },
    {
      "_id": "5cdf75c3fcf79425c8c6ee02",
      "name": "Android App Development",
      "category": "workshop"
    }
    ...
    ...
  ]
  ```

## Get Event
### GET <code>/events/:id</code>
Get all details of a single event specified by id in url parameter.

#### Example Request
URL <code>/events/5cdf75c3fcf79425c8c6ee02</code>

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "booked_count": 0,
    "published": true,
    "_id": "5cdf58b3ef30d8125c4f2c9c",
    "name": "Android App Development",
    "meta": null,
    "booking_limit": 60,
    "description": null,
    "category": "workshop"
  }
  ```
- Bug on non-existant but valid id in url<br>
  *Status* 200 OK
  ```
  null
  ```
- Failure on Invalid id in url (id has non-hexadecimal charecters or has invalid length) <br>
  *Status* 500 Internal Server Error
  ```
  {
    "message": "Cast to ObjectId failed for value "5cdf58b3ef30d8125c4f2c6z" at path "_id" for model "event"",
    "status": 500,
    "stack": "CastError: Cast to ObjectId failed for value "5cdf58b3ef30d8125c4f2c6z" at path "_id" for model "event" at new CastError (/home/salih/Projects/proton/node_modules/mongoose/lib/error/cast.js:29:11) at ObjectId.cast (/home/salih/Projects/proton/node_modules/mongoose/lib/schema/objectid.js:242:11) at ObjectId.SchemaType.applySetters....."
  }
  ```

## Change Publicity
### POST <code>/events/publicity</code>
Publicity denotes weather the event is visible to participants. An event can be published only if certain important fields are filled. Eg. Event should have a maximum limit of bookings allowed before publishing.
An event can be unpublished unconditionally.

### Request Body Parameters

| Field | Type | Description | Required? |
|--|--|--|--|
| _id | string | ID of the event to publish / unpublish | Yes |
| published | boolean | Set true to publish, false to unpublish | Yes |

#### Example Request Body
```
{
  "_id": "5cdf58b3ef30d8125c4f2c9c",
  "published": true
}
```

#### Example Responses
- Success <br>
  *Status* 200 OK
  ```
  {
    "message": "Published"
  }
  ```
